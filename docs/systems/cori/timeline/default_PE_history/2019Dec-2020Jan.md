# Programming Environment Change on Cori in December 2019 and January 2020

## Background

During the scheduled maintenances on Cori on Dec 5-6, 2019, we
upgraded the OS from CLE7.0UP00 to CLE7.0UP01 (mainly a Lustre client
version update), and installed the new Cray Programming Environment
Software release
[CDT/19.11](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.11_November_7_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.11_November_7_2019.pdf).
There were no software default versions change at the Dec maintenance.
CDT/19.11 were set to default on Jan 14, 2020, during the Allocation
Year Transition.

Below is the detailed list of changes. 

## New software versions available

Note: These new versions are available as non-default from Dec 5, 2019
and have been set as default on Jan 14, 2020 at the Allocation Year
Transition.

* cce/9.1.0
* cray-R/3.6.1
* cray-ccdb/3.0.5
* cray-cti/1.0.9
* cray-fftw/3.3.8.4
* cray-hdf5, cray-hdf5-parallel/1.10.5.2
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.10
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.3.2
* cray-parallel-netcdf/1.11.1.1
* cray-shmem/7.7.10
* craype/2.6.2
* craype-dl-plugin-py2, craype-dl-plugin-py3/19.09.1
* iobuf/2.0.9
* modules/3.2.11.4
* papi/5.7.0.2
* perftools, perftools-base, perftools-lite/7.1.1

## Important Information Regarding CCE 9 Compatibility

In
[CDT/19.06](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.06_June_20_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.06_June_20_2019.pdf)
or later.

Note: CCE/9.1.0 is the default version after Jan 14, 2020.

Cray compiler users should note the important information about the
all-new CCE 9.0; in particular, the CCE 9.0 C/C++ compiler is based on
Clang instead of the classic Cray compiler. Key consequences of these
changes include:

* CCE 9.0 compilers are not compatible with pre-CDT-19.06 library
  versions (such as MPI)
* The OpenMP flag is no longer turned on by default.

CCE 9.0 released in CDT/19.06 includes changes that require updated
versions of many libraries, including MPI, for compatibility with CCE
9.0.  The libraries in the June/19.06 (or later) PE release are
compatible with CCE 9.0, but are not compatible with CCE 8.7.  If a
CCE user needs to go back to a product from a previous PE release,
they should change both CCE and MPI, and any other dependent
libraries.  For example, CCE 9.0 would use the CDT/19.06 PE library
cray-mpich 7.7.8 or later, while CCE 8.7 would use cray-mpich 7.7.7 or
earlier.

Also, the CCE 9.0 C and C++ compiler is now based on Clang/LLVM.
Users need to adjust makefiles to use Clang command line syntax, which
is similar to the GNU compiler syntax.  Fortran is not affected.  The
previous C and C++ compiler, now referred to as CCE Classic C and C++,
is available with CCE 9.0 to support a transition period.  CCE Classic
C and C++ will be discontinued in a future release.

It is recommended that CCE users read the CCE 9.0 (or later) release
notes for more detailed compatibility information. The CCE 9.1.0
release notes may be viewed using the command "module help cce/9.1.0".

## Default linking mode change

in
[CDT/19.06](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.06_June_20_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.06_June_20_2019.pdf)
and later

Beginning with the PE 19.06 release, the default linking mode on XC
Cray systems becomes dynamic.  This change is being made in version
2.6.0 of craype, and applies to all versions of all compilers,
including CCE.  Static linking will still be a non-default option,
where supported.  Applications that have already been built are
unaffected by this change.

From the time that the change is made in craype, anything linked will
be linked dynamically by default.  Thus, swapping to a craype version
prior to cdt/19.06 will result in static linking by default; while
swapping to the cdt/19.06 version (or later) of craype will result in
dynamic linking by default.

The default can be overridden either on the command line, with the
'-static' flag, or by setting 'CRAYPE_LINK_TYPE=static' in the
environment.

Please see the detailed information in
[CDT/19.06](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.06_June_20_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.06_June_20_2019.pdf)
or
[CDT/19.11](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.11_November_7_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.11_November_7_2019.pdf)
release notes.
