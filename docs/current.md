# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource. While we will attempt to
    make the system available to users as much as possible, it is
    subject to unannounced and unexpected outages, reconfigurations,
    and periods of restricted access. Please visit the [timeline
    page](systems/perlmutter/timeline/index.md) for more information
    about changes we've made in our recent upgrades.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

### New issues

- MPICH returns null bytes when collectively reading with MPI-IO from
  a file on `/pscratch` using progressive file layout (PFL) and
  exceeding a boundary between the different striping factors. If you
  want to use PFL, you can use `export
  MPICH_MPIIO_HINTS="*:romio_cb_read=disable"` to disable collective
  reading.

### Ongoing issues

- Our slingshot 11 libfabric (Perlmutter CPU nodes) is currently missing the functions
  `MPI_Mprobe()/MPI_Improbe()` and `MPI_Mrecv()/MPI_Imrecv()`. This
  may especially impact mpi4py users attempting to send/receive pickled
  objects. One workaround may be to set `export MPI4PY_RC_RECV_MPROBE='False'`.
  The current estimated timeframe for a fix is January 2023.
  If these missing functions are impacting your workload, please open a ticket
  to let us know.
- MPI users may hit `segmentation fault` errors when trying
  to launch an MPI job with many ranks due to incorrect
  allocation of GPU memory. We provide [more information
  and a suggested workaround](systems/perlmutter/index.md#known-issues-with-cuda-aware-mpi).
- You may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- Shifter user may see errors about `BIND MOUNT FAILED` if they
  attempt to volume mount directories that are not world
  executable. We have [some workarounds for this
  issue](../development/shifter/issues/#invalid-volume-map).
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix from the vendor soon. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

### Past Issues

For updates on past issues affecting Perlmutter, see the
[timeline page](systems/perlmutter/timeline/index.md).

## Cori

### Ongoing issues

- The Burst Buffer on Cori has a number of known issues, documented at
  [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues): its
  usage is discouraged.
