# Accounts

## Obtaining a user account

In order to use the NERSC facilities, you need:

 1. A user account with an associated user login name (also called a username).
 2. Access to a project with an allocation of computational or storage resources. 

If you are not a member of a project that already has a NERSC allocation award
and you have a research grant that supports the mission of the [DOE Office of
Science](https://www.nersc.gov/users/accounts/allocations/allocation-managers/), 
you may apply for a project allocation. Please see [Applying for your 
First Allocation](https://www.nersc.gov/users/accounts/allocations/first-allocation/). 
In particular, Exploratory Awards are available for new projects that wish to 
investigate using NERSC resources, or who wish to port or develop new codes.

### How to get a New User account in an existing project/repository

 1. You can submit a request for a new NERSC user account by using the
[Create a NERSC Account](https://iris.nersc.gov/add-user) form.
 2. Using the "I need a new NERSC account" button, enter your preferred username, 
the project you are applying to join, contact information, and organization. 
For best results, ask your project PI for the name of the project to use (project 
names are often the letter "m" followed by some (usually four) digits, e.g., m1234).
 3. Your account will undergo user vetting, in accordance with NERSC policies,
to verify your identity. Under some circumstances, there could be a delay of up
to a week while this vetting takes place.
 4. After vetting has completed, the PI and PI Proxies for the project will be 
notified that your account request has been submitted. They will review your 
account request in Iris and either approve or deny your request. If they approve 
your request, you will receive an email notification that you have been added to 
a project. If your request is denied, the PI/Proxy will enter a reason for denying 
the request and you will receive an email with this reason. You should then take
the requested actions or contact them to resolve the issue.
 5. You will also receive an email with a link that will allow you to set
your initial password. That link will expire if not used within 24 hours. If
the link has expired, there will be an additional link in the email to obtain 
a new link and code to set a password.
 6. Finally, you will need to log into the [Iris](https://iris.nersc.gov) website to set up
Multi-Factor Authentication (MFA) for your account. You can log into Iris with just 
your username and password. Once logged in, you will be requested to accept the 
NERSC Appropriate Use Policies. You can then set up your MFA following this documentation:
[Multi-Factor Authentication](../connect/mfa.md)

### If you have an existing NERSC user account and want to join another project

If you are an existing NERSC user and need to be added to a new/another project,
the project PI or one of their proxies can add you. You must know the name of the PI
and the name of the Project/repository. You can then either:

 1. Contact the PI and request that they add your account to their project 

OR

 2. Go to the [NERSC New Account Request](https://iris.nersc.gov/add-user) page and 
click on the "I have a current NERSC account" button, enter your "Existing Username"
and then search for the project by either entering the PI's name or the Project Name 
in the text box. The PI or one of their Proxies will need to approve your request and
when that is done, you will receive an email notification

## Managing Accounts

NERSC user accounts are managed in the [Iris](https://iris.nersc.gov) system.
For more information on how to use Iris, please see the
[Iris documentation](../iris/iris-for-users.md).
