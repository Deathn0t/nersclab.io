# Workflow queue

To support workflows on the Perlmutter system, we offer a `workflow` 
[scrontab](../workflow-tools.md#scrontab) queue for
users to submit lightweight tasks which coordinate computational work.

## Getting access to the workflow queue

To request access to the Perlmutter workflow queue, please fill out the
[Workflow Queue Request Form](https://help.nersc.gov/) at the NERSC help desk.

To help us decide if your use case is appropriate for the workflow queue, you
will be required to enter:

```
User:
Email:
Project name:
Statement of purpose:
Estimated memory usage:
Estimated CPU usage:
Estimated data usage:
Estimated I/O:
Frequency / length of process:
Need external resources (yes/no):
```

## Example workflow scrontab script

Scripts must include traditional Slurm flags like `-q`, `-A`, and
`-t`. Below is an an example workflow scrontab script with a walltime of 30
days that will run once every hour. Note the
`#SCRON --open-mode=append` line, which instructs Slurm
to _append_ any new output to the output file:

```
#SCRON -q workflow
#SCRON -A <account>
#SCRON -t 30-00:00:00
#SCRON -o output-%j.out
#SCRON --open-mode=append
0 */1 * * * <full_path_to_your_script>
```

!!! warning "Scrontab times are in UTC"
    Currently, scrontab times on Perlmutter are in UTC. 

## Workflow queue details

Jobs in the workflow queue may request a walltime of up to 90 days and up to
one quarter of the resources (CPU and/or memory) of a Perlmutter login node.

## Known issues

If an scrontab job is cancelled for any reason (e.g., by the user or ahead of
an upcoming maintenance), the user must manually reenable the job by editing
their scrontab file with `scrontab -e` and removing comment characters which
were inserted by Slurm upon job cancellation.
